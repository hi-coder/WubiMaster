﻿namespace WubiMaster.Common;

public class Bool2Orientation : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        try
        {
            if (value == null) return Orientation.Horizontal;
            bool _value = (bool)value;
            return _value ? Orientation.Horizontal : Orientation.Vertical;
        }
        catch (Exception)
        {
            return Orientation.Horizontal;
        }
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        return value;
    }
}

public class Bool2Reverse : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        try
        {
            if (value == null) return true;
            bool _value = bool.Parse(value.ToString());
            return _value ? false : true;
        }
        catch (Exception)
        {
            return true;
        }
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        return value;
    }
}

public class SearchTextToVisible : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (value == null) return Visibility.Visible;
        else if (value.ToString().Length == 0) return Visibility.Visible;
        else return Visibility.Collapsed;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (value == null) return Visibility.Visible;
        else if (value.ToString().Length == 0) return Visibility.Visible;
        else return Visibility.Collapsed;
    }
}

public class Str2Brush : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}

public class Text2Visibility : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (value == null) return Visibility.Collapsed;
        string v = value.ToString();
        if (string.IsNullOrEmpty(v)) return Visibility.Collapsed;
        return Visibility.Visible;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        return value;
    }
}

public class WinState2Angle : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        try
        {
            if (value == null) return 0.0;

            var layoutValue = value.ToString().ToLower();
            if (layoutValue == "right") return 180.0;
            return 0.0;
        }
        catch (Exception ex)
        {
            LogHelper.Error(ex.ToString());
            return 0.0;
        }
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        return value;
    }
}

public class WubiType2Visibility : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (value == null) return Visibility.Collapsed;
        if (value.ToString() == parameter.ToString() || value.ToString() == "全部") return Visibility.Visible;
        return Visibility.Collapsed;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        return value;
    }
}